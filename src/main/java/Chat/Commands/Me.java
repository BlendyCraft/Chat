package Chat.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Chat.Chat;
import Chat.Utilities.PlayerUtils;
import Chat.Utilities.RateLimit.Limit;
public class Me implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) return true;
		
		try {
			if (Chat.getInstance().getRateLimiter().isLimited("command_me", (Player)sender)) {
				((Player)sender).spigot().sendMessage(
					Chat.getInstance().getRateLimiter().createLimitedMessage()
				);
				
				return true;
			}
			
		} catch (Exception e) { }
		
		Bukkit.getOnlinePlayers().forEach((player) -> {
			player.spigot().sendMessage(
				PlayerUtils.createSelfMessage(String.join(" ", args), (Player)sender)
			);
		});
		
		return true;
	}
	
	public Me() {
		try {
			
			Chat.getInstance().getRateLimiter().addLimit(
				new Limit("command_me", 1000)
			);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}