package Chat.Commands;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Chat.Chat;
import Chat.Utilities.PlayerUtils;
import Chat.Utilities.RateLimit.Limit;

public class Message implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (args.length == 1)
      return false;
      
    if (sender instanceof Player) {
      try {
			  if (Chat.getInstance().getRateLimiter().isLimited("command_message", (Player)sender)) {
				  ((Player)sender).spigot().sendMessage(
					  Chat.getInstance().getRateLimiter().createLimitedMessage()
				  );
				
				  return true;
			  }
			
      } catch (Exception e) { }
    }

    Object reciever = PlayerUtils.getPlayerByName(args[0]);

    String senderName = 
      (sender instanceof Player) ? ((Player)sender).getName() : "Server";

    String recieverName = 
      (reciever instanceof Player) ? ((Player)sender).getName() : ((OfflinePlayer)sender).getName();

    if (reciever == null) {
      sender.spigot().sendMessage(
        PlayerUtils.getNotFoundMessage(recieverName));

      return true;
    }

    if (reciever instanceof OfflinePlayer) {
      sender.spigot().sendMessage(
        PlayerUtils.getOfflineMessage((OfflinePlayer)reciever));

      return true;
    }

    sender.spigot().sendMessage(
      PlayerUtils.createSentMessage(args[1], recieverName, reciever));

    ((Player)reciever).spigot().sendMessage(
      PlayerUtils.createRecievedMessage(args[1], senderName, sender));

    return true;
  }
  
  public Message() {
    try {

      Chat.getInstance().getRateLimiter().addLimit(
        new Limit("command_message", 500)
      );

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}