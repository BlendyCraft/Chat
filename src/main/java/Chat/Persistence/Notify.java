package Chat.Persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.ArrayList;

import org.bukkit.entity.Player;

import Chat.Chat;

import org.bukkit.OfflinePlayer;

public class Notify {
  public static class Notification {
    private Object sender;
    private Object reciever;
    private String message;
    private boolean hasSeen;
    
    public Object getSender() {
      return this.sender;
    }
    
    public Object getReciever() {
      return this.reciever;
    }
    
    public String getMessage() {
      return this.message;
    }
    
    public boolean getHasSeen() {
      return this.hasSeen;
    }
    
    public Notification(Object sender, Object reciever, String message) {
      this.sender = sender;
      this.reciever = reciever;
      this.message = message;
    }
    
    public Notification(Object sender, Object reciever, String message, boolean hasSeen) {
      this.sender = sender;
      this.reciever = reciever;
      this.message = message;
      this.hasSeen = hasSeen;
    }
  }
  
  public static void doDatabaseSetup() {
    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "CREATE TABLE IF NOT EXISTS `Notifications` (" + 
          "`id` INT NOT NULL," +
          "`sender` VARCHAR(36)," + 
          "`reciever` VARCHAR(36)," + 
          "`sent` TIMESTAMP DEFAULT NOW()," + 
          "`seen` TIMESTAMP DEFAULT NOW()," + 
          "`hasSeen` BOOL DEFAULT FALSE," +
          "`message` VARCHAR(255)," +
          "PRIMARY KEY(`id`)" + 
        ");");

      Statement.executeUpdate();
      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();
      
    } finally {
      MySQL.close(Statement, conn);
    }
  }
  
  public static List<Notification> get(Object player) {
    List<Notification> retVal = new ArrayList<>();
    
    UUID id = (player instanceof Player) ? ((Player) player).getUniqueId() 
      : ((OfflinePlayer) player).getUniqueId();

    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement("SELECT * FROM Notifications WHERE reciever=?;");

      Statement.setString(1, id.toString());

      ResultSet result = Statement.executeQuery();

      while (result.next()) {
        Player onPlayer = 
          Chat.getInstance().getServer().getPlayer(UUID.fromString(result.getString("sender")));
          
        Object sender = (onPlayer != null) ? 
          Chat.getInstance().getServer().getPlayer(UUID.fromString(result.getString("sender"))) :
          Chat.getInstance().getServer().getOfflinePlayer(UUID.fromString(result.getString("sender")));
        
        retVal.add(new Notification(
          sender, 
          player, 
          result.getString("message"),
          result.getBoolean("hasSeen")
        ));
      }

      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }

    return retVal;
  }
  
  public static List<Notification> getUnread(Object player) {
    List<Notification> retVal = new ArrayList<>();
    
    UUID id = (player instanceof Player) ? ((Player) player).getUniqueId() 
      : ((OfflinePlayer) player).getUniqueId();

    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "SELECT * FROM Notifications WHERE reciever=? AND hasSeen=FALSE;"
      );

      Statement.setString(1, id.toString());

      ResultSet result = Statement.executeQuery();

      while (result.next()) {
        Player onPlayer = 
          Chat.getInstance().getServer().getPlayer(UUID.fromString(result.getString("sender")));
          
        Object sender = (onPlayer != null) ? 
          Chat.getInstance().getServer().getPlayer(UUID.fromString(result.getString("sender"))) :
          Chat.getInstance().getServer().getOfflinePlayer(UUID.fromString(result.getString("sender")));
        
        retVal.add(new Notification(
          sender, player, 
          result.getString("message"),
          false
        ));
      }

      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }

    return retVal;
  }
  
  public static boolean exists(Notification notification) {
    boolean exists = false;
    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "SELECT * FROM Notifications WHERE sender=? AND reciever=? AND message=?;"
      );
      
      Statement.setString(1, notification.getSender().toString());
      Statement.setString(2, notification.getReciever().toString());
      Statement.setString(3, notification.getMessage());
      
      ResultSet result = Statement.executeQuery();
      
      exists = result.next();
      
      MySQL.close(Statement, conn);
      
    } catch (SQLException e) {
      e.printStackTrace();
      
    } finally {
      MySQL.close(Statement, conn);
    }
    
    return exists;
  }
  
  public boolean send(Notification notification) {
    if (exists(notification)) return false;
    
    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "INSERT INTO Notifications (sender, reciever, message) VALUES (?, ?, ?);"
      );
      
      Statement.setString(1, notification.getSender().toString());
      Statement.setString(2, notification.getReciever().toString());
      Statement.setString(3, notification.getMessage());
      Statement.executeUpdate();
      
      MySQL.close(Statement, conn);
      
    } catch (SQLException e) {
      e.printStackTrace();
    
    } finally {
      MySQL.close(Statement, conn);
    }
    
    return true;
  }
  
  public static void markRead(Notification notification) {
    if (!exists(notification)) return;
    
    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "UPDATE Notifications SET hasSeen=TRUE WHERE sender=? AND reciever=? AND message=?;"
      );
      
      Statement.setString(1, notification.getSender().toString());
      Statement.setString(2, notification.getReciever().toString());
      Statement.setString(3, notification.getMessage());
      Statement.executeUpdate();
      
      MySQL.close(Statement, conn);
      
    } catch(SQLException e) {
      e.printStackTrace();
      
    } finally {
      MySQL.close(Statement, conn);
    }
  }
}