package Chat.Persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.configuration.file.FileConfiguration;

import Chat.Chat;

public class MySQL {
  /**
   * Close all opened connections immediately
   * @return Database connection
   */
  public static Connection getConnection() {
    Connection result = null;

    try {
      result = DriverManager.getConnection(
        String.format(
          "jdbc:mysql://localhost:3306/%s?useSSL=true",
          getConfig().getString("MySQL.Data")
        ),
        getConfig().getString("MySQL.User"),
        getConfig().getString("MySQL.Pass")
      );


    } catch (SQLException ex) { ex.printStackTrace(); }

    return result;
  }
  
  public static Boolean test() {
    Connection conn = getConnection();
    Boolean result = (conn != null);
    close(conn);
    return result;
  }

  /**
   * Attempts to close a supplied connection properly
   *
   * @param connection Connection to close
   */
  public static void close(Connection connection) {
    close(null, null, connection);
  }

  /**
   * Attempts to close a supplied connection and prepared
   * statement properly
   *
   * @param statement Statement to close
   * @param connection Connection to close
   */
  public static void close(Statement statement, Connection connection) {
    close(null, statement, connection);
  }

  /**
   * Attempts to close a supplied connection and result set properly
   *
   * @param resultSet Result set to close
   * @param connection Connection to close
   */
  public static void close(ResultSet resultSet, Connection connection) {
    close(resultSet, null, connection);
  }

  /**
   * Attempts to close a supplied connection, statement and
   * result set properly
   *
   * @param resultSet Result set to close
   * @param statement Statement to close
   * @param connection Connection to close
   */
  public static void close(ResultSet resultSet, Statement statement, Connection connection) {
    if (connection != null) {
      try {
        connection.close();

      } catch (SQLException e) { e.printStackTrace(); }
    }

    if (statement != null) {
      try {
        statement.close();

      } catch (SQLException e) { e.printStackTrace(); }
    }

    if (resultSet != null) {
      try {
        resultSet.close();

      } catch (SQLException e) { e.printStackTrace(); }
    }
  }
  
  private static FileConfiguration getConfig() {
    return Chat.getInstance().getConfig();
  }
}