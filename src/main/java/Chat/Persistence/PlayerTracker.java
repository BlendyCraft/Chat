package Chat.Persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import Chat.Utilities.PlayerUtils;

public class PlayerTracker {
  public static List<PlayerData> OnlinePlayers = new ArrayList<>();
  
  public static class PlayerData {
    UUID id;
    Timestamp joined;
    Timestamp seen;
    String confirmation;
    Boolean confirmed;
    
    public UUID getId() {
      return this.id;
    }
    
    public Timestamp getJoined() {
      return this.joined;
    }
    
    public Timestamp getSeen() {
      return this.seen;
    }
    
    public String getConfirmation() {
      return this.confirmation;
    }
    
    public Boolean getConfirmed() {
      return this.confirmed;
    }
    
    public PlayerData(UUID id, Timestamp joined, Timestamp seen, String confirmation, Boolean confirmed) {
      this.id = id;
      this.joined = joined;
      this.seen = seen;
      this.confirmation = confirmation;
      this.confirmed = confirmed;
    }
  }
  
  public static void doDatabaseSetup() {
    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "CREATE TABLE IF NOT EXISTS `Players` (" + 
          "`uuid` VARCHAR(36)," + 
          "`joined` TIMESTAMP DEFAULT NOW()," + 
          "`seen` TIMESTAMP DEFAULT NOW()," + 
          "`confirmation` VARCHAR(36)," +
          "`confirmed` BOOL DEFAULT FALSE," +
          "PRIMARY KEY(`uuid`)" + 
        ");");

      Statement.executeUpdate();
      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();
      
    } finally {
      MySQL.close(Statement, conn);
    }
  }
  
  public static PlayerData getPlayerData(Object player) {
    PlayerData retVal = null;
    UUID id = (player instanceof Player) ? ((Player)player).getUniqueId() : ((OfflinePlayer)player).getUniqueId();
    
    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement("SELECT * FROM Players WHERE uuid=?;");

      Statement.setString(1, id.toString());
      ResultSet result = Statement.executeQuery();

      while (result.next()) {
        retVal = new PlayerData(
          UUID.fromString(result.getString("uuid")), 
          result.getTimestamp("joined"), 
          result.getTimestamp("seen"), 
          result.getString("confirmation"), 
          result.getBoolean("confirmed")
        );
      }
      
      MySQL.close(Statement, conn);
      
      return retVal;

    } catch (SQLException e) {
      e.printStackTrace();
      
      return null;

    } finally {
      MySQL.close(Statement, conn);
    }
  }
  
  public static Boolean playerExists(Object player) {
    Boolean doesExist = false;
    UUID id = (player instanceof Player) ? ((Player)player).getUniqueId() : ((OfflinePlayer)player).getUniqueId();
    
    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "SELECT * FROM Players WHERE uuid=?;"
      );
      
      Statement.setString(1, id.toString());

      ResultSet result = Statement.executeQuery();

      doesExist = result.next();

      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }
    
    return doesExist;
  }
  
  public static void insertPlayer(Object player) {
    if (playerExists(player)) return;
    
    UUID id = (player instanceof Player) ? ((Player) player).getUniqueId() : ((OfflinePlayer) player).getUniqueId();
    
    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "INSERT INTO Players (uuid, joined, seen, confirmation) VALUES (?, NOW(), NOW(), ?);"
      );
      
      Statement.setString(1, id.toString());
      Statement.setString(2, PlayerUtils.generateRandomString(32));
      Statement.execute();
      
      MySQL.close(Statement, conn);
      
    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }
  }
  
  public static void updatePlayer(Object player) {
    if (!playerExists(player)) return;
      
    UUID id = (player instanceof Player) ? ((Player)player).getUniqueId() : ((OfflinePlayer)player).getUniqueId();

    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "UPDATE Players SET seen=Now() WHERE uuid=?;"
      );
      
      Statement.setString(1, id.toString());
      Statement.execute();
      
      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }
  }
  
  public static String getConfirmation(Object player) {
    if (!playerExists(player)) return null;
    
    String confirmation = null;
    
    UUID id = (player instanceof Player) ? ((Player)player).getUniqueId() : ((OfflinePlayer)player).getUniqueId();
    
    Connection conn = null;
    PreparedStatement Statement = null;
    
    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "SELECT * FROM Players WHERE uuid=?;"
      );
      
      Statement.setString(1, id.toString());
      ResultSet result = Statement.executeQuery();
      
      if (!result.next())
        confirmation = null;
        
      confirmation = result.getString("confirmation");
      
      MySQL.close(Statement, conn);
      
    } catch (SQLException e) {
      e.printStackTrace();
    
    } finally {
      MySQL.close(Statement, conn);
    }
    
    return confirmation;
  }
  
  public static Boolean checkConfirmationCode(Object player, String confirmation) {
    String internalConfirmation = getConfirmation(player);
    
    UUID id = (player instanceof Player) ? ((Player)player).getUniqueId() 
      : ((OfflinePlayer)player).getUniqueId();
    
    if (internalConfirmation == null) return false;
    
    if (confirmation.equals(internalConfirmation)) {
      Connection conn = null;
      PreparedStatement Statement = null;
      
      try {
        conn = MySQL.getConnection();
        Statement = conn.prepareStatement(
          "UPDATE Players SET confirmed=TRUE WHERE uuid=?;"
        );
        
        Statement.setString(1, id.toString());
        Statement.execute();
        
        MySQL.close(Statement, conn);
        
      } catch (SQLException e) {
        e.printStackTrace();
        
      } finally {
        MySQL.close(Statement, conn);
      }
      
      return true;
    }
    
    return false;
  }
  
  public static void queuePlayer(Player player) {
    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement("SELECT * FROM Players WHERE uuid=?;");

      Statement.setString(1, player.getUniqueId().toString());
      ResultSet result = Statement.executeQuery();

      while (result.next()) {
        OnlinePlayers.add(new PlayerData(
          UUID.fromString(result.getString("uuid")), 
          result.getTimestamp("joined"), 
          result.getTimestamp("seen"), 
          result.getString("confirmation"), 
          result.getBoolean("confirmed")
        ));
      }
      
      MySQL.close(Statement, conn);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }
  }
  
  public static void dequeuePlayer(Player player) {
    Iterator<PlayerData> iter = OnlinePlayers.iterator();

    while (iter.hasNext()) {
      PlayerData data = iter.next();

      if (data.getId().equals(player.getUniqueId()))
        iter.remove();
        
    }
  }
  
  public static PlayerData getQueuedPlayerData(Player player) {
    for (PlayerData playerData : OnlinePlayers) {
      if (playerData.getId().equals(player.getUniqueId())) 
        return playerData;
        
    }
    
    return null;
  }
}