package Chat.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import Chat.Persistence.PlayerTracker;
import Chat.Persistence.PlayerTracker.PlayerData;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;

public class PlayerUtils {
  public static class TaggedMessage {
    String[] words;
    List<Player> tagged;
    
    public TaggedMessage(String[] words, List<Player> tagged) {
      this.words = words;
      this.tagged = tagged;
    }
  }
  
  /**
   * Gets the Object representing a player, offline or not.
   * Returns {@link #Player} if online and {@link #OfflinePlayer} 
   * if offline.
   */
  public static Object getPlayerByName(String name) {
    return
      Bukkit.getPlayer(name) != null ? 
        Bukkit.getPlayer(name) : 
        Bukkit.getOfflinePlayer(name);

  }

  /**
   * Generates a random string with a predefined length
   */
  public static String generateRandomString(int length) {
    char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
    StringBuilder sb = new StringBuilder(length);
    Random random = new Random();
    
    for (int i = 0; i < 20; i++) {
      char c = chars[random.nextInt(chars.length)];
      sb.append(c);
    }
    
    return sb.toString();
  }
  
  /**
   * Adds @ for tagging to player names inside of a string
   * then returns that {@link #TaggedMessage}
   */
  public static TaggedMessage getHighlightedPlayerNamesFromString(String message) {
    String[] words = message.split(" ");
    List<Player> tagged = new ArrayList<>();
    
    for (int i = 0; i < words.length; i++) {
      final int z = i;
      Bukkit.getOnlinePlayers().forEach((player) -> {
        if (player.getName().equalsIgnoreCase(words[z])) {
          words[z] = String.format("@%s", player.getName());
        }
      });
    }
    
    return new TaggedMessage(words, tagged);
  }
  
  /**
   * Returns a {@link #HoverEvent} for chat that 
   * provides some information about the player on hover.
   */
  public static HoverEvent getPlayerHover(Object player) {
    boolean online = (player instanceof Player);
    
    String playerName = online ? ((Player)player).getName() 
      : ((OfflinePlayer)player).getName();
      
    PlayerData playerData = null;
    
    if (player instanceof Player)
      playerData = PlayerTracker.getQueuedPlayerData((Player)player);
      
    if (playerData != null) {
      String seen = online ? "Online"
          : new java.text.SimpleDateFormat("MM.dd.yyyy hh:mmaaa z").format(playerData.getSeen());
          
      return new HoverEvent(
        HoverEvent.Action.SHOW_TEXT,
        new ComponentBuilder(playerName)
          .color(ChatColor.BLUE)
          .append("\nSeen: ")
          .color(ChatColor.YELLOW)
          .append(seen)
          .color(online ? ChatColor.GREEN : ChatColor.WHITE)
          .append("\nJoined: ")
          .color(ChatColor.YELLOW)
          .append(new java.text.SimpleDateFormat("MM.dd.yyyy").format(playerData.getJoined()))
          .color(ChatColor.WHITE)
          .append("\nClick here to tag ")
          .append("@" + playerName)
          .color(ChatColor.AQUA)
        .create()
      );
    }
    
    return null;
  }
  
  /**
   * Returns a {@link #ClickEvent} for attaching to 
   * a player's global message.
   */
  public static ClickEvent getGlobalPlayerClick(Object player) {
    String playerName = 
      (player instanceof Player) ? ((Player)player).getName() : ((OfflinePlayer)player).getName();
    
    return new ClickEvent(
      Action.SUGGEST_COMMAND,
      String.format("@%s ", playerName)
    );
  }
  
  /**
   * Returns a {@link #ClickEvent} for attaching to 
   * recieved private message text.
   */
  public static ClickEvent getPrivatePlayerClick(Object player) {
    String playerName = 
      (player instanceof Player) ? ((Player) player).getName() : ((OfflinePlayer) player).getName();

    return new ClickEvent(
      Action.SUGGEST_COMMAND, 
      String.format("/m %s ", playerName)
    );
  }

  /**
   * Returns a {@link #BaseComponent} to send as a chat message 
   * when a player is not found on the server.
   */
  public static BaseComponent[] getNotFoundMessage(String name) {
    return new ComponentBuilder(name)
      .color(ChatColor.WHITE)
      .append(" was not found!")
      .color(ChatColor.DARK_GRAY)
    .create();
  }

  /**
   * Returns a {@link #BaseComponent} to send as a chat message 
   * when a player is offline.
   */
  public static BaseComponent[] getOfflineMessage(OfflinePlayer player) {
    return new ComponentBuilder(player.getName())
      .event(getPlayerHover(player))
      .color(ChatColor.WHITE)
      .append(" is currently offline!")
      .color(ChatColor.DARK_GRAY)
    .create();
  }

  /**
   * Returns a {@link #BaseComponent} to send as a chat message 
   * to a player that has just sent a message via private messaging.
   * This lets the player know that their message was sent and 
   * ensures that screenshots taken by either participant can contain 
   * the full conversation.
   */
  public static BaseComponent[] createSentMessage(String message, String reciever, Object pObj) {
    return new ComponentBuilder("[")
      .color(ChatColor.WHITE)
      .append(reciever)
      .event(PlayerUtils.getPlayerHover(pObj))
      .color(ChatColor.DARK_GREEN)
      .append("] ")
      .color(ChatColor.WHITE)
      .append(message)
      .color(ChatColor.DARK_GRAY)
    .create();
  }

  /**
   * Returns a {@link #BaseComponent} to send as a chat message 
   * to a player recieving a private message sent to them.
   */
  public static BaseComponent[] createRecievedMessage(String message, String sender, Object pObj) {
    return new ComponentBuilder("[")
      .color(ChatColor.WHITE)
      .append(sender)
      .event(PlayerUtils.getPlayerHover(pObj))
      .event(PlayerUtils.getPrivatePlayerClick(pObj))
      .color(ChatColor.AQUA)
      .append("] ")
      .color(ChatColor.WHITE)
      .append(message)
      .color(ChatColor.DARK_GRAY)
    .create();
  }
  
  /**
   * Creates a {@link #BaseComponent} to send a chat message
   * as a global message to everyone on the server. Includes 
   * player hover and player click events.
   */
  public static BaseComponent[] createGlobalMessage(String message, Player sender) {
    return new ComponentBuilder("[")
      .color(ChatColor.GRAY)
      .append(sender.getName())
      .event(PlayerUtils.getPlayerHover(sender))
      .event(PlayerUtils.getGlobalPlayerClick(sender))
      .color(ChatColor.BLUE)
      .append("] ")
      .color(ChatColor.GRAY)
      .append(message)
      .color(ChatColor.WHITE)
    .create();
  }
  
  /**
   * Creates a {@link #BaseComponent} to send a chat message
   * as a global message to everyone on the server. Includes 
   * player hover and player click events.
   */
  public static BaseComponent[] createGlobalMessage(String message, Plugin sender, HoverEvent hover, ClickEvent click) {
    return new ComponentBuilder("[")
      .color(ChatColor.GRAY)
      .append(sender.getName())
      .event(hover)
      .event(click)
      .color(ChatColor.YELLOW)
      .append("] ")
      .color(ChatColor.GRAY)
      .append(message)
      .color(ChatColor.WHITE)
    .create();
  }
  
  /**
   * Creates a {@link #BaseComponent} to send a chat message 
   * as a global message to everyone on the server as a self 
   * action aka. /me command from Bukkit revamped.
   */
  public static BaseComponent[] createSelfMessage(String message, Player sender) {
    return new ComponentBuilder(String.format("* %s %s", sender.getName(), message))
      .color(ChatColor.GRAY)
      .event(PlayerUtils.getPlayerHover(sender))
      .event(PlayerUtils.getGlobalPlayerClick(sender))
    .create();
  }
}