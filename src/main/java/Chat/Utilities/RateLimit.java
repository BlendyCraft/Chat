package Chat.Utilities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class RateLimit {
  private Plugin plugin;
  private List<Limit> limiters;
  
  public static class Limit {
    private String tag;
    private int interval;
    private HashMap<Player, Timestamp> limitTable;
    
    public String getTag() {
      return this.tag;
    }
    
    public int getInterval() {
      return this.interval;
    }
    
    public HashMap<Player, Timestamp> getLimitTable() {
      return this.limitTable;
    }
    
    /**
     * A limit is a certain category of rate limit 
     * identified by it's String tag. You can use 
     * a created Limit object throughout your code 
     * for a single limit. Each limit holds a table 
     * of players and the last timestamp that was 
     * recorded so you should create a limit for 
     * each thing you need to limit individually.
     * 
     * @param tag The tag to identify this limit
     * @param interval The interval, in milliseconds, to limit
     */
    public Limit(String tag, int interval) {
      this.tag = tag;
      this.interval = interval;
      this.limitTable = new HashMap<>();
    }
  }
  
  /**
   * Gets a limiter based on it's tag
   * 
   * @param Tag to find limit for
   * 
   * @return {@link #Limit} identified by this tag
   */
  private Limit getLimiterByTag(String tag) {
    for (Limit limiter : this.limiters) {
      if (limiter.tag.equals(tag)) {
        return limiter;
      }
    }
    
    return null;
  }
  
  /**
   * Adds a limit to your plugins limit cache. This 
   * is intended to keep track of limits that you 
   * have defined througout your plugin. You should 
   * use this instead of defining your own cache.
   * 
   * @param limiter The limit to add
   */
  public void addLimit(Limit limiter) throws Exception {
    if (this.limiters.contains(limiter)) {
      throw new Exception("Limit already exists!");
    }
    
    for (Limit existingLimiter : this.limiters) {
      if (existingLimiter.tag.equals(limiter.tag)) {
        throw new Exception(String.format("Limit with tag \"%s\" already exists!", limiter.tag));
      }
    }
    
    this.limiters.add(limiter);
  }
  
  /**
   * Removes a limit from your plugins limit cache. This 
   * is intended to keep track of limits that you 
   * have defined throughout your plugin. You should 
   * use this instead of defining your own cache.
   * 
   * @param limiter The limit to remove
   */
  public void removeLimit(Limit limiter) throws Exception {
    if (!this.limiters.contains(limiter)) {
      throw new Exception("Limit does not exist!");
    }
    
    this.limiters.remove(limiter);
  }
  
  /**
   * Checks if a player is limited by a specific limit 
   * that has been previously added to your limit cache 
   * with {@link #addLimit(Limit)}. Please note that 
   * this method will automatically add or update the 
   * last used timestamp of the player for this limit 
   * to make it easier to use this helper class.
   * 
   * @param limiter The limit to check in
   * @param player The player to check for
   * 
   * @return {@link #boolean} value for if the player is currently limited
   */
  public boolean isLimited(Limit limiter, Player player) throws Exception {
    if (!this.limiters.contains(limiter)) {
      throw new Exception("Limit does not exist!");
    }
    
    Timestamp now = new Timestamp(System.currentTimeMillis());
    Timestamp lastUsed = limiter.getLimitTable().get(player);
    
    if (lastUsed == null) {
      limiter.getLimitTable().put(player, now);
      return false;
    }
      
    // Update player used timestamp for this limit
    limiter.getLimitTable().put(player, now);
    
    return ((now.getTime() - lastUsed.getTime()) < limiter.getInterval());
  }
  
  /**
   * Checks if a player is limited by a specific limit 
   * that has been previously added to your limit cache 
   * with {@link #addLimit(Limit)}. Please note that
   * this method will automatically add or update the 
   * last used timestamp of the player for this limit 
   * to make it easier to use this helper class.
   * 
   * @param limitTag The tag of the limit to check in
   * @param player  The player to check for
   * 
   * @return {@link #boolean} value for if the player is currently limited
   */
  public boolean isLimited(String limitTag, Player player) throws Exception {
    Limit limiter = getLimiterByTag(limitTag);
    
    if (limiter == null) {
      throw new Exception("Limit does not exist!");
    }
    
    return isLimited(limiter, player);
  }
  
  /**
   * Get a message object to send to the player telling 
   * them that they have been rate limited. Please 
   * use this to provide consistency within the plugin.
   * 
   * @return {@link #BaseComponent} array representing a message object
   */
  public BaseComponent[] createLimitedMessage() {
    return new ComponentBuilder("[")
      .color(ChatColor.GRAY)
      .append(this.plugin.getName())
      .color(ChatColor.BLUE)
      .append("] ")
      .color(ChatColor.GRAY)
      .append("Slow Down! You've been rate limited.")
      .color(ChatColor.WHITE)
      .create();
  }
  
  /**
   * Create new {@link #RateLimit(Plugin)} container 
   * for rate limiting anything based on a {@link #Player} 
   * and a {@link #Timestamp}.
   * 
   * @param plugin The plugin creating the object
   */
  public RateLimit(Plugin plugin) {
    this.plugin = plugin;
    this.limiters = new ArrayList<>();
  }
}