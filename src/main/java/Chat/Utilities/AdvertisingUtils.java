
/*
 * Right now the only way to run an ad is to insert one via 
 * the MySQL command line program in terminal.
 * 
 * Example:
 *  INSERT INTO Advertisements (impressions, maxImpressions, expires, type, message, actLink, hovMessage, expired) VALUES (0, 100, NULL, 'open', 'Found a bug? Click here to report!', 'https://gitlab.com/groups/BlendyCraft/-/issues', 'Report bugs on the Gitlab issue tracker!', FALSE);
 */

package Chat.Utilities;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;

import Chat.Chat;
import Chat.Persistence.MySQL;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class AdvertisingUtils {
  public class Advertisement {
    int id;
    int impressions;
    int maxImpressions;
    Date expires;
    String type;
    String message;
    String actLink;
    String hovMessage;
    Boolean expired;
    
    public Advertisement(String message, String actLink, String hovMessage) {
      this.message = message;
      this.actLink = actLink;
      this.hovMessage = hovMessage;
    }
    
    public Advertisement(Date expires, String message, String actLink, String hovMessage) {
      this.expires = expires;
      this.message = message;
      this.actLink = actLink;
      this.hovMessage = hovMessage;
    }
    
    public Advertisement(int maxImpressions, String message, String actLink, String hovMessage) {
      this.maxImpressions = maxImpressions;
      this.message = message;
      this.actLink = actLink;
      this.hovMessage = hovMessage;
    }
    
    public Advertisement(int maxImpressions, Date expires, String message, String actLink, String hovMessage) {
      this.maxImpressions = maxImpressions;
      this.expires = expires;
      this.message = message;
      this.actLink = actLink;
      this.hovMessage = hovMessage;
    }
    
    public Advertisement(int id, int impressions, int maxImpressions, Date expires, String type, String message, String actLink, String hovMessage, Boolean expired) {
      this.id = id;
      this.impressions = impressions;
      this.maxImpressions = maxImpressions;
      this.type = type;
      this.expires = expires;
      this.message = message;
      this.actLink = actLink;
      this.hovMessage = hovMessage;
      this.expired = expired;
    }
  }

  private Random random;
  private List<Advertisement> RunningAdvertisements;

  private void doDatabaseSetup() {
    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement(
        "CREATE TABLE IF NOT EXISTS `Advertisements` (" +
          "`id` INT(6) NOT NULL AUTO_INCREMENT," +
          "`impressions` INT(6)," +
          "`maxImpressions` INT(12)," +
          "`expires` DATE," +
          "`type` VARCHAR(10)," +
          "`message` VARCHAR(255)," +
          "`actLink` VARCHAR(255)," +
          "`hovMessage` VARCHAR(255)," +
          "`expired` BOOL DEFAULT FALSE," +
          "PRIMARY KEY(`id`)" +
        ");"
      );

      Statement.executeUpdate();
      MySQL.close(conn);

    }
    catch (SQLException e) { e.printStackTrace(); } 
    finally { MySQL.close(Statement, conn); }
  }

  private List<Advertisement> readDatabase() {
    List<Advertisement> ads = new ArrayList<>();

    Connection conn = null;
    PreparedStatement Statement = null;

    try {
      conn = MySQL.getConnection();
      Statement = conn.prepareStatement("SELECT * FROM Advertisements WHERE `expired`=FALSE;");

      ResultSet result = Statement.executeQuery();

      while (result.next()) {
        ads.add(new Advertisement(
          result.getInt("id"),
          result.getInt("impressions"),
          result.getInt("maxImpressions"),
          result.getDate("expires"),
          result.getString("type"),
          result.getString("message"),
          result.getString("actLink"),
          result.getString("hovMessage"),
          result.getBoolean("expired")
        ));
      }

      MySQL.close(conn);

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      MySQL.close(Statement, conn);
    }

    return ads;
  }

  private void loadCurrentAdvertisements() {
    RunningAdvertisements.clear();
    RunningAdvertisements.addAll(readDatabase());
  }

  private void createTask() {
    Chat.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(Chat.getInstance(), new Runnable() {
      @Override
      public void run() {
        Bukkit.getOnlinePlayers().forEach((player) -> {
          player.spigot().sendMessage(
            getAdvertisementMessage(RunningAdvertisements.get(random.nextInt(RunningAdvertisements.size())))
          );
        });
      }
    }, 0L, 3600L);
  }

  public AdvertisingUtils() {
    doDatabaseSetup();
    RunningAdvertisements = new ArrayList<>();
    loadCurrentAdvertisements();
    random = new Random();
    createTask();
  }
  
  private static HoverEvent getAdvertisementHover(Advertisement ad) {
    if (ad.hovMessage == null) return null;
    
    return new HoverEvent(
      HoverEvent.Action.SHOW_TEXT, 
      new ComponentBuilder(ad.hovMessage).create()
    );
  }
  
  private static ClickEvent getAdvertisementClick(Advertisement ad) {
    if (ad.actLink == null) return null;
    
    switch (ad.type.toLowerCase()) {
      case "suggest":
        return new ClickEvent(
          ClickEvent.Action.SUGGEST_COMMAND, ad.actLink
        );
      
      case "run":
        return new ClickEvent(
          ClickEvent.Action.RUN_COMMAND, ad.actLink
        );
        
      default:
        return new ClickEvent(
          ClickEvent.Action.OPEN_URL, ad.actLink
        );
    }
  }
  
  public static BaseComponent[] getAdvertisementMessage(Advertisement ad) {
    return new ComponentBuilder("[")
      .color(ChatColor.GRAY)
      .append("Advertisement")
      .event(getAdvertisementHover(ad))
      .event(getAdvertisementClick(ad))
      .color(ChatColor.YELLOW)
      .append("] ")
      .color(ChatColor.GRAY)
      .append(ad.message)
      .color(ChatColor.WHITE)
      .create();
  }
}