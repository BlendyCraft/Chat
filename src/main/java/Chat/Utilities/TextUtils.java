package Chat.Utilities;

import java.util.Random;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class TextUtils {
  /**
   * Gets a random ChatColor, including colors that may 
   * be difficult to see in game!
   * @return Random ChatColor
   */
  public static ChatColor getRandomChatColor() {
    ChatColor[] Rando = new ChatColor[] { 
      ChatColor.AQUA, ChatColor.BLACK, ChatColor.BLUE, ChatColor.DARK_AQUA,
      ChatColor.DARK_BLUE, ChatColor.DARK_GRAY, ChatColor.DARK_GREEN, ChatColor.DARK_PURPLE,
      ChatColor.DARK_RED, ChatColor.GOLD, ChatColor.GRAY, ChatColor.GREEN, ChatColor.LIGHT_PURPLE,
      ChatColor.RED, ChatColor.WHITE, ChatColor.YELLOW
    };

    return Rando[new Random().nextInt(Rando.length)];
  }

  /**
   * Gets a random ChatColor within rainbow colors
   * @return Random rainbow ChatColor
   */
  public static ChatColor getRainbowChatColor() {
    ChatColor[] Rainbow = new ChatColor[] {
      ChatColor.RED, ChatColor.YELLOW, ChatColor.GREEN, 
      ChatColor.BLUE, ChatColor.DARK_BLUE, ChatColor.DARK_PURPLE
    };

    return Rainbow[new Random().nextInt(Rainbow.length)];
  }

  /**
   * Creates random colored text for chat box from a string
   * This includes colors that are difficult to see in the game!
   * @param text Text to add random colors to
   * @return Random colored text
   */
  public static TextComponent createRandomColorText(String text) {
    TextComponent RandomColorText = new TextComponent();

    for (int i = 0; i < text.length(); i++) {
      TextComponent ColoredLetter = 
        new TextComponent(String.valueOf(text.charAt(i)));

      ColoredLetter.setColor(getRandomChatColor());

      RandomColorText.addExtra(ColoredLetter);
    }

    return RandomColorText;
  }

  /**
   * Creates rainbow colored text for chat box from a string
   * @param text Sring to rainbowify (it's a word..)
   * @return Rainbow colored text
   */
  public static TextComponent createRainbowColorText(String text) {
    TextComponent RainbowColorText = new TextComponent();

    for (int i = 0; i < text.length(); i++) {
      TextComponent ColoredLetter = 
        new TextComponent(String.valueOf(text.charAt(i)));

      ColoredLetter.setColor(getRainbowChatColor());

      RainbowColorText.addExtra(ColoredLetter);
    }

    return RainbowColorText;
  }
}