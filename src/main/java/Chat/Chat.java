package Chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import Chat.Commands.Me;
import Chat.Commands.Message;
import Chat.Persistence.MySQL;
import Chat.Persistence.Notify;
import Chat.Persistence.PlayerTracker;
import Chat.Utilities.AdvertisingUtils;
import Chat.Utilities.PlayerUtils;
import Chat.Utilities.RateLimit;

public class Chat extends JavaPlugin implements Listener {
    private static Chat Instance;
    private static AdvertisingUtils Ads;
    
    private RateLimit rateLimit;

    public static Chat getInstance() {
        return Instance;
    }
    
    public RateLimit getRateLimiter() {
        return this.rateLimit;
    }
    
    @Override
    public void onEnable() {
        Instance = this;

        saveDefaultConfig();
        
        if (!MySQL.test())
            System.out.println("Error: Couldn't get MySQL connection. Check config.yml");
            
        Notify.doDatabaseSetup();
        PlayerTracker.doDatabaseSetup();
        
        rateLimit = new RateLimit(this);

        getCommand("me").setExecutor(new Me());
        getCommand("message").setExecutor(new Message());

        getServer().getPluginManager().registerEvents(this, this);

        Ads = new AdvertisingUtils();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChat(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
        
        Bukkit.getOnlinePlayers().forEach((player) -> {
            player.spigot().sendMessage(
                PlayerUtils.createGlobalMessage(
                    event.getMessage(), 
                    event.getPlayer()
                )
            );
        });
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(PlayerDeathEvent event) {
        event.setDeathMessage("");
        Bukkit.getOnlinePlayers().forEach((player) -> {
            player.spigot().sendMessage(
                PlayerUtils.createSelfMessage("Died!", (Player)event.getEntity())
            );
        });
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage("");
        
        PlayerTracker.queuePlayer(event.getPlayer());
        
        if (!PlayerTracker.playerExists(event.getPlayer()))
            PlayerTracker.insertPlayer(event.getPlayer());
        
        PlayerTracker.updatePlayer(event.getPlayer());
        
        Bukkit.getOnlinePlayers().forEach((player) -> {
            player.spigot().sendMessage(
                PlayerUtils.createSelfMessage("Joined!", event.getPlayer())
            );
        });
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage("");
        PlayerTracker.dequeuePlayer(event.getPlayer());
        PlayerTracker.updatePlayer(event.getPlayer());
        Bukkit.getOnlinePlayers().forEach((player) -> {
            player.spigot().sendMessage(
                PlayerUtils.createSelfMessage("Left!", event.getPlayer())
            );
        });
    }
}
