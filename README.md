## Chat: Super Simple Yet Robust Spigot Chat System
Chat was developed by @Cryptobyte as a plugin for small servers. It is designed to be fast and lightweight yet meet the requirements for small and medium servers.

### Configuration
- Upload Chat.jar to your server `plugins/` directory
- Run server once and then `stop` the server
- Edit `plugins/Chat/config.yml` and add your MySQL details
- Start the server and confirm MySQL connection in Terminal

---

### Components
- __Global Chat__
  is configured as the default chat space. No commands are required to speak in Global Chat. We refer to Global Chat as the ability to communicate with all online players at once.
  
- __Private Messaging__
  is a useful feature for having private conversations between players. Chat has Private Messaging mapped to the `/m` `/pm` and `/msg` commands. Private Messaging does not currently support offline players however this is an upcoming feature.
  
- __Advertisements__
  in Chat utilize all of the modern Chat functions of Spigot therefore allowing click and hover actions. You can define these actions and each advertisement inside the database. Advertisements are meant to be used for server wide announcements that run every 3 minutes at random. Random advertisements will be chosen by the plugin to run on scedule.
  
- __Notify__
  are a simple and easy to use method of sending a notification to an online or offline player. The idea is to provide a message like component that automatically and gracefully handles offline players as well as online ones. Notifications can be any `String` sent by someone to be recieved by another. This is developed as a SDK component for other plugins to implement and as a component that Chat references internally, as such all methods can be called statically from outside of the Chat plugin itself. Notify implements simple and easy to understand methods as well as a `Notification` object that should be passed and is returned in certain instances. Please take a look at the Wiki if you are interested in using this component in your plugin.
 
---

### Special Effects
Chat supports modern Spigot chat features such as hover and click events without making them too flashy. The idea is to keep a minimal approach with these features added. Clicking on different messages trigger various actions such as clicking on a player's message to Global Chat will auto suggest `@player ` in the chat window to make it easier to tag that player and clicking on an incoming private message will auto suggest `/m player ` to make it easier to quickly reply to that player.